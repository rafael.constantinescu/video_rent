 use renting_movies;
create table users(
userId int auto_increment primary key,
firstName varchar(50) not null, 
lastName varchar(50) not null,
dateOfBirth date not null,
bonus double,
sighnInDate date
);

select * from users;
insert into users(firstName, lastName, dateOfBirth, bonus, sighnInDate)
values
("Doe","John","2000-08-12","2.5","2020-11-14"),
("Yann", "Yannis","1999-05-11","3.5","2020-11-14"),
("Jim", "James","1997-03-18","5.5","2020-11-14"),
("Yan", "Yaniss","1991-05-11","3.5","2020-11-14"),
("Ben", "Benny","2001-01-08","1.5","2020-11-14"),
("Georgiana","Geo","2002-03-15","0.5","2020-11-14"),
("Gina", "Ginnet","1999-05-11","3.5","2020-11-14"),
("Brian", "Garcia","1995-04-23","6.5","2020-11-14"),
("Dan", "Donn","1985-02-09","6.5","2020-11-14"),
("yohanna", "Linn","1997-01-14","3.5","2020-11-14");

