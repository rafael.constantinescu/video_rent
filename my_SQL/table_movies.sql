startDate
CREATE TABLE `renting_movies`.`movies` (
  `movieId` INT NOT NULL AUTO_INCREMENT,
  `movieName` VARCHAR(45) NULL,
  `movieRegizor` VARCHAR(45) NULL,
  `movieDescriptiom` VARCHAR(200) NULL,
  `movieRating` INT(2) NULL,
  `moviePrice` INT(4) NULL,
  PRIMARY KEY (`movieId`));
INSERT INTO `renting_movies`.`movies` (`movieId`, `movieName`, `movieDescriptiom`, `movieRating`, `moviePrice`) VALUES ('1', 'Twoo Popes', 'Reality', '5', '50');
INSERT INTO `renting_movies`.`movies` (`movieId`, `movieName`, `movieDescriptiom`, `movieRating`, `moviePrice`) VALUES ('2', 'Breaking Bad', 'Drama', '5', '100');
INSERT INTO `renting_movies`.`movies` (`movieId`, `movieName`, `movieDescriptiom`, `movieRating`, `moviePrice`) VALUES ('3', 'Snatch', 'Reality', '4', '50');
INSERT INTO `renting_movies`.`movies` (`movieId`, `movieName`, `movieDescriptiom`, `movieRating`, `moviePrice`) VALUES ('4', 'The Queen\'s Gambit', 'Reality', '5', '50');
INSERT INTO `renting_movies`.`movies` (`movieId`, `movieName`, `movieDescriptiom`, `movieRating`, `moviePrice`) VALUES ('5', 'Blow', 'Reality', '4', '50');
INSERT INTO `renting_movies`.`movies` (`movieId`, `movieName`, `movieDescriptiom`, `movieRating`, `moviePrice`) VALUES ('6', 'Jandarmul si extraterestrii', 'Comedy', '5', '50');
INSERT INTO `renting_movies`.`movies` (`movieId`, `movieName`, `movieDescriptiom`, `movieRating`, `moviePrice`) VALUES ('7', 'The Hangover 2', 'Comedy', '5', '50');
INSERT INTO `renting_movies`.`movies` (`movieId`, `movieName`, `movieDescriptiom`, `movieRating`, `moviePrice`) VALUES ('8', 'The Hangover', 'Comedy', '5', '50');
INSERT INTO `renting_movies`.`movies` (`movieId`, `movieName`, `movieDescriptiom`, `movieRating`, `moviePrice`) VALUES ('9', 'Star Wars', 'SF', '5', '50');
INSERT INTO `renting_movies`.`movies` (`movieId`, `movieName`, `movieDescriptiom`, `movieRating`, `moviePrice`) VALUES ('10', 'Theory of Everything', 'Reality', '5', '50');

alter table movies add column movieGenre varchar(20);
 alter table movies add column movieYear int not null;
 alter table movies add column ageLimit int not null;
 
 