use renting_movies;

create table rentings (
rentingId INT auto_increment primary key,
startDate date,
endDate date,
userId int,
movieId int,
discount double);
alter table rentings add constraint fk_renting_to_user foreign key(userId) references users(userId);
 alter table rentings add constraint fk_renting_to_movie foreign key(movieId) references movies(movieId);

insert into rentings(startDate, userId, movieId, discount)
values
("2020-10-11",1, 3, 0.9),
("2020-10-11",4, 6, 0.9),
("2020-10-12",1, 7, 0.9),
("2020-10-12",3, 5, 0.9),
("2020-10-12",7, 1, 0.8),
("2020-10-14",1, 9, 0.7),
("2020-10-14",6, 3, 0.9),
("2020-10-14",1, 3, 0.9),
("2020-10-14",1, 1, 0.9),
("2020-10-14",1, 8, 0.9);

UPDATE rentings SET endDate = "2020-10-14" WHERE rentingId =1;
UPDATE rentings SET endDate = "2020-10-14" WHERE rentingId =2;
UPDATE rentings SET endDate = "2020-10-15" WHERE rentingId =3;
UPDATE rentings SET endDate = "2020-10-15" WHERE rentingId =4;
UPDATE rentings SET endDate = "2020-10-16" WHERE rentingId =5;
UPDATE rentings SET endDate = "2020-10-17" WHERE rentingId =7;

alter table rentings add column rentingPrice double;