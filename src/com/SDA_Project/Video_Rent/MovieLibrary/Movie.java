package com.SDA_Project.Video_Rent.MovieLibrary;

import javax.persistence.*;

@Entity
@Table(name = "movies")
public class Movie {
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name = "movieId")
    private Integer movieId;
    @Column(name = "movieName")
    private String movieName;
    @Column(name = "movieYear")
    private Integer movieYear;
    @Column(name = "movieGenre")
    private String genre;
    @Column(name = "movieRegizor")
    private Integer movieRegizor;
    @Column(name = "movieDescriptiom")
    private String description;
    @Column(name = "moviePrice")
    private int price;

//    public Movie( String movieName, int movieYear, String genre) {
//
//        this.setMovieName(movieName);
//        this.setMovieYear(movieYear);
//        this.setGenre(genre);
//    }

    private  Integer getMovieId() {
        return this.movieId;
    }

    private  void setMovieId(Integer movieId) {
        this.movieId = movieId;
    }

    public String getMovieName() {
        return this.movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setMoviePrice(int moviePrice) {
        this.price = moviePrice;
    }

    public int getMoviePrice() {
        return this.price;
    }
 public int getMovieYear() {
        return this.movieYear;
    }

    public void setMovieYear(Integer movieYear) {
        this.movieYear = movieYear;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Integer getMovieRegizor() {
        return movieRegizor;
    }

    public void setMovieRegizor(Integer movieRegizor) {
        this.movieRegizor = movieRegizor;
    }


    @Override
    public String toString() {
        return "Movie{" +
                "movieId=" + movieId +
                ", movieName='" + movieName + '\'' +
                ", movieYear=" + movieYear +
                ", genre='" + genre + '\'' +
                ", movieRegizor=" + movieRegizor +
                ", description='" + description + '\'' +
                ", price=" + price +
                '}';
    }
}
