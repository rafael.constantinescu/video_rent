package com.SDA_Project.Video_Rent;

import java.util.Properties;

import com.SDA_Project.Video_Rent.MovieLibrary.Movie;
import com.SDA_Project.Video_Rent.RentingActivity.RentingMovie;
import com.SDA_Project.Video_Rent.UserGroup.User;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;


// trebuie importata librariea Maven :  org.hibernate: hiberante-core:5.2.12.Final
    public class HibernateUtils {
        private static SessionFactory sessionFactory;

        public static SessionFactory getSessionFactory(){
            if(sessionFactory == null) {
                try {
                    Configuration configuration = new Configuration();
                    Properties settings = new Properties();
                    // set settings
                    settings.put(Environment.DRIVER, "com.mysql.jdbc.Driver");
                    settings.put(Environment.URL, "jdbc:mysql://144.91.88.122:3307/renting_movies");
//                    Pentru Dan si Mihai
//                    settings.put(Environment.URL, "jdbc:mysql://localhost:3306/renting_movies");
                    settings.put(Environment.USER, "root");
                    settings.put(Environment.PASS, "MySQL_1234");
                    settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5Dialect");
                    settings.put(Environment.SHOW_SQL, "true");
                    //set settings to configuration
                    configuration.setProperties((settings));

                    configuration.addAnnotatedClass((Movie.class));
                    configuration.addAnnotatedClass((User.class));
                    configuration.addAnnotatedClass((RentingMovie.class));

                    ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                            .applySettings(configuration.getProperties())
                            .build();
                    sessionFactory = configuration.buildSessionFactory(serviceRegistry);
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
            return sessionFactory;
        }
    }


