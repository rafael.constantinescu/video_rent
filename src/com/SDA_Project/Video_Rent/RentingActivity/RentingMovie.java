package com.SDA_Project.Video_Rent.RentingActivity;
import com.SDA_Project.Video_Rent.MovieLibrary.Movie;
import com.SDA_Project.Video_Rent.UserGroup.User;

import javax.persistence.*;
import java.time.LocalDate;


@Entity
@Table(name ="rentings")
public class RentingMovie {

@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer rentingId;

@Column(name = "startDate")
    private LocalDate rentStartDate;
@Column(name = "endDate")
    private LocalDate rentEndDate;
@Column(name = " userId")
    private int  userId;
@Column(name = "movieId")
    private int movieId;
@Column(name = "discount")
    private double discount;
@Column(name = "rentingPrice")
    private Double price;

// POJO Classes no need constructor!!!!!

//    public RentingMovie(LocalDate rentStartDate, int userId, int movieId) {
//
//        this.rentStartDate = rentStartDate;
//        this.userId = userId;
//        this.movieId = movieId;
//    }

    public void setRentStartDate(LocalDate rentStartDate) {
        this.rentStartDate = rentStartDate;
    }

    public LocalDate getRentStartDate() {
        return rentStartDate;
    }

    public void setRentEndDate(LocalDate rentEndDate) {
        this.rentEndDate = rentEndDate;
    }


    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public Double getPrice() {
        return price;
    }

    private boolean isReturned(){
        if (this.rentEndDate != null){
            return true;
        }
            return false;
    }
    private boolean userIsPermit(User user, Movie movie){
//        if (user.getAge()> movie.)
        return true;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "RentingMovie{" +
                "rentingId=" + rentingId +
                ", rentStartDate=" + rentStartDate +
                ", rentEndDate=" + rentEndDate +
                ", userId=" + userId +
                ", movieId=" + movieId +
                ", discount=" + discount +
                ", price=" + price +
                '}';
    }
}
