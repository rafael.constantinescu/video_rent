package com.SDA_Project.Video_Rent.RentingActivity;

import com.SDA_Project.Video_Rent.HibernateUtils;
import com.SDA_Project.Video_Rent.UserGroup.UserList;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class RentingList {
    //    Singleton RentingList for reflecting all rentals and their status
    private static RentingList rentings = null;
    List<RentingMovie> list;

    private RentingList() {
        this.list = new ArrayList<>();
    }

    public static RentingList createRentingList() {
        if (rentings == null) {
            rentings = new RentingList();
        }
        return rentings;
    }
    public void update(){
        Session session = HibernateUtils.getSessionFactory().openSession();
        String selectAllRentingsSQL = "from RentingMovie";// numele clasei adnotate
        Query selectAllRentingsQuery = session.createQuery(selectAllRentingsSQL);
        this.list = selectAllRentingsQuery.list();
        session.close();

    }
    public static void showAll(){
        rentings.update();
        for(RentingMovie item: rentings.list) {
            System.out.println(item.toString());
        }
    }

    public void newRent(RentingMovie renting) {
//        rentings.setRentStartDate(LocalDate.now());
//        this.list.add(renting);
    }

    public void endRent(RentingMovie renting, String endDate) {
        renting.setRentEndDate(LocalDate.parse(endDate));
    }
}