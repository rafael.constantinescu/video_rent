package com.SDA_Project.Video_Rent;

import com.SDA_Project.Video_Rent.MovieLibrary.MovieLibrary;
import com.SDA_Project.Video_Rent.RentingActivity.RentingList;
import com.SDA_Project.Video_Rent.RentingActivity.RentingMovie;
import com.SDA_Project.Video_Rent.UserGroup.UserList;

public class Main {

    public static void main(String[] args) {
	// write your code here
        RentingList rentings = RentingList.createRentingList();
        rentings.showAll();
        System.out.println("*****************************************************************");
        UserList users = UserList.creatUserList();
        users.showAll();
        System.out.println("*****************************************************************");
        MovieLibrary movies = MovieLibrary.createMovieList();
          movies.showAll();
    }
}
