package com.SDA_Project.Video_Rent.UserGroup;

import com.SDA_Project.Video_Rent.HibernateUtils;
import com.SDA_Project.Video_Rent.RentingActivity.RentingMovie;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class UserList {

    private static UserList userList = null;
    private List<User> list;

    private UserList() {
        this.list = new ArrayList<>();
    }

    public static UserList creatUserList() {
        if (userList == null) {
            userList = new UserList();
        }
        return userList;
    }

    public void update() {
        Session session = HibernateUtils.getSessionFactory().openSession();
        String selectAllUsersHQL = "from User";// numele clasei adnotate
        Query selectAllUsersQuery = session.createQuery(selectAllUsersHQL);
        this.list = selectAllUsersQuery.list();
        session.close();
    }
    public void showAll(){
        userList.update();
        for(User item: userList.list) {
            System.out.println(item.toString());
        }
    }

    public void addUser(User user){
        this.list.add(user);
    }


}

