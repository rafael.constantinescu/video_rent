package com.SDA_Project.Video_Rent.UserGroup;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDate;
import java.time.Period;
import javax.persistence.*;

@Entity
@Table(name = "users")

public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer userId;

    @Column(name = "firstName")
    private String firstName;

    @Column(name = "lastName")
    private String lastName;

    @Column(name = "dateOfBirth")
    private LocalDate dateOfBirth;  // Modificat Date to LocalDate pt easy Age calculation

    @Column(name = "bonus")
    private Double bonus;

    @Column(name = "sighnInDate")
    private LocalDate signInDate;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

// POJO Classes no need constructor!!!!!
//    private User(String firstName, String lastName, LocalDate dateOfBirth) {
//        this.setFirstName(firstName);
//        this.setLastName(lastName);
//        this.setDateOfBirth(dateOfBirth);
//    }

    public int getAge(LocalDate birthDate) {
        LocalDate dateNow = LocalDate.of(2020, 9, 11);
        Period period = Period.between(dateNow, birthDate);
        return Math.abs(period.getYears());
    }


    public Double getBonus() {
        return bonus;
    }

    public void setBonus(Double bonus) {
        this.bonus = bonus;
    }


    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public LocalDate getSignInDate() {
        return signInDate;
    }

    private void setLastName(String lastName) {
        this.lastName = lastName;
    }

    private void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    private void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    private void setSignInDate(LocalDate signInDate) {
        this.signInDate =  signInDate;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", bonus=" + bonus +
                ", signInDate=" + signInDate +
                '}';
    }
}
